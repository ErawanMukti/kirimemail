<?php

use \Phalcon\Mvc\Model\Behavior\SoftDelete;

class Siswa extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="nama", type="string", length=25, nullable=false)
     */
    public $nama;

    /**
     *
     * @var string
     * @Column(column="no_tlp", type="string", length=15, nullable=false)
     */
    public $no_tlp;

    /**
     *
     * @var string
     * @Column(column="gender", type="string", length=1, nullable=false)
     */
    public $gender;

    /**
     *
     * @var string
     * @Column(column="nama_bapak", type="string", length=25, nullable=false)
     */
    public $nama_bapak;

    /**
     *
     * @var string
     * @Column(column="tlp_bapak", type="string", length=15, nullable=false)
     */
    public $tlp_bapak;

    /**
     *
     * @var string
     * @Column(column="nama_ibu", type="string", length=25, nullable=false)
     */
    public $nama_ibu;

    /**
     *
     * @var string
     * @Column(column="tlp_ibu", type="string", length=15, nullable=false)
     */
    public $tlp_ibu;

    /**
     *
     * @var string
     * @Column(column="alamat_ortu", type="string", nullable=false)
     */
    public $alamat_ortu;

    /**
     *
     * @var string
     * @Column(column="email_ortu", type="string", nullable=false)
     */
    public $email_ortu;

    /**
     *
     * @var integer
     * @Column(column="status", type="integer", length=11, nullable=false)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(column="created_at", type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(column="updated_at", type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("kirimemail");
        $this->setSource("siswa");
        $this->addBehavior(new SoftDelete(
            array(
                'field' => 'status',
                'value' => 0
            )
        ));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'siswa';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Siswa[]|Siswa|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Siswa|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function beforeCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
        $this->updated_at = date("Y-m-d H:i:s");
    }

    public function beforeUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }
}
