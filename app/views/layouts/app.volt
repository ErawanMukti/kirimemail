<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Kirim.Email</title>

        <!-- Styles -->
        {{ stylesheet_link('css/bootstrap.min.css') }}
        {{ stylesheet_link('css/dataTables.bootstrap.min.css') }}
    </head>

    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        {{ link_to('', 'Kirim.Email', 'class' : 'navbar-brand') }}
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            <li>{{ link_to('', 'Home') }}</li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="container">
                <div class="row">
                    {% block content %}{% endblock %}
                </div>
            </div>
        </div>

        <!-- Scripts -->
        {{ javascript_include('js/app.js') }}
        {{ javascript_include('js/jquery-1.12.4.js') }}
        {{ javascript_include('js/jquery.dataTables.min.js') }}
        {{ javascript_include('js/dataTables.bootstrap.min.js') }}
        <script>
            $(document).ready(function() {
                $('#tbsiswa').DataTable({
                    "ordering": false,
                });
            });
        </script>        
    </body>
</html>
