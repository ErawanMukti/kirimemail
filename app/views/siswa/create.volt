{% extends 'layouts/app.volt' %}

{% block content %}
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            Informasi Siswa
            <i class="small"> (Siswa Baru)</i>
        </div>
        <div class="panel-body">
            {{ flashSession.output() }}
            {{ form('siswa/store/', 'class':'form-horizontal') }}
                <div class="form-group">
                    <label for="nama" class="col-md-3 control-label">Nama Siswa</label>
                    <div class="col-md-9">
                        {{ text_field('nama', 'class':'form-control') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="no_tlp" class="col-md-3 control-label">No. Telepon</label>
                    <div class="col-md-9">
                        {{ text_field('no_tlp', 'class':'form-control') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="gender" class="col-md-3 control-label">Jenis Kelamin</label>
                    <div class="col-md-9">
                        {{ select('gender', [
                            'P': 'Pria',
                            'W': 'Wanita'
                        ], 'class':'form-control') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_bapak" class="col-md-3 control-label">Nama Ayah</label>
                    <div class="col-md-9">
                        {{ text_field('nama_bapak', 'class':'form-control') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="tlp_bapak" class="col-md-3 control-label">Telepon Ayah</label>
                    <div class="col-md-9">
                        {{ text_field('tlp_bapak', 'class':'form-control') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_ibu" class="col-md-3 control-label">Nama Ibu</label>
                    <div class="col-md-9">
                        {{ text_field('nama_ibu', 'class':'form-control') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="tlp_ibu" class="col-md-3 control-label">Telepon Ibu</label>
                    <div class="col-md-9">
                        {{ text_field('tlp_ibu', 'class':'form-control') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat_ortu" class="col-md-3 control-label">Alamat Orang tua</label>
                    <div class="col-md-9">
                        {{ text_field('alamat_ortu', 'class':'form-control') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="email_ortu" class="col-md-3 control-label">Email Orang Tua</label>
                    <div class="col-md-9">
                        {{ email_field('email_ortu', 'class':'form-control') }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-md-offset-3">
                        {{ submit_button('Simpan', 'class':'btn btn-success') }}
                        {{ link_to('siswa', 'Kembali', 'class':'btn btn-primary') }}
                    </div>
                </div>
            {{ endForm() }}
        </div>
    </div>
</div>
{% endblock %}
