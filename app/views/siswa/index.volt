{% extends 'layouts/app.volt' %}

{% block content %}
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Daftar Siswa
            <div class="navbar-right">
                {{ link_to('siswa/create', 'Tambah Data', 'class':'btn btn-success') }}
                &nbsp;&nbsp;&nbsp;
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            {{ flashSession.output() }}
            <table id="tbsiswa" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th class="col-md-2 col-sm-2 col-xs-12">Nama</th>
                        <th class="col-md-2 col-sm-2 col-xs-12">Telepon</th>
                        <th class="col-md-5 col-sm-4 col-xs-12">Alamat</th>
                        <th class="col-md-1 col-sm-1 col-xs-12">Gender</th>
                        <th class="col-md-2 col-sm-2 col-xs-12"></th>
                    </tr>
                </thead>
                <tbody>
                    {% for siswa in data %}
                    <tr>
                        <td>{{ siswa.nama }}</td>
                        <td>{{ siswa.no_tlp }}</td>
                        <td>{{ siswa.alamat_ortu }}</td>
                        <td>
                            {% if (siswa.gender == 'P') %}
                                Pria
                            {% else %}
                                Wanita
                            {% endif %}
                        </td>
                        <td>
                            {{ link_to('siswa/edit/'~siswa.id, 'Ubah', 'class':'btn btn-primary') }}
                            {{ link_to('siswa/destroy/'~siswa.id, 'Hapus', 'class':'btn btn-danger') }}
                        </td>
                    </tr>
                    {% endfor  %}
                </tbody>
            </table>
        </div>
    </div>
</div>
{% endblock %}