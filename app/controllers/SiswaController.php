<?php

class SiswaController extends ControllerBase
{

    public function indexAction()
    {
    	$this->view->data = Siswa::find('status = 1');
    }

    public function createAction() { }

    public function storeAction()
    {
    	$input = $this->request->getPost();

    	$siswa = new Siswa();
    	$stored = $siswa->create($_POST);

    	if ($stored) {
    		$this->flashSession->success('Data Siswa Berhasil Ditambah');
            return $this->response->redirect('siswa/index');
    	} else {
    		$this->flashSession->error('Proses Tambah Data Gagal');
            return $this->response->redirect('siswa/create/');
    	}
    }

    public function showAction($id) { }

    public function editAction($id)
    {
    	$data = Siswa::findFirst($id);

    	if ( !$data ) {
			$this->flashSession->error('Data Siswa Tidak Ditemukan');
            return $this->response->redirect('siswa/index');
    	} else {
    		$this->tag->setDefault('gender', $data->gender);
	    	$this->view->data = $data;
    	}
    }

    public function updateAction($id)
    {
    	$input = $this->request->getPost();

    	$siswa = Siswa::find($id);
    	$updated = $siswa->update($_POST);

    	if ($updated) {
    		$this->flashSession->success('Data Siswa Berhasil Diupdate');
            return $this->response->redirect('siswa/index');
    	} else {
    		$this->flashSession->error('Proses Update Gagal');
            return $this->response->redirect('siswa/edit/'.$id);
    	}
    }

    public function destroyAction($id)
    {
    	$siswa = Siswa::find($id);
    	$deleted = $siswa->delete();

    	if ($deleted) {
    		$this->flashSession->success('Data Siswa Berhasil Dihapus');
    	} else {
    		$this->flashSession->error('Proses Hapus Gagal');
    	}
        return $this->response->redirect('siswa/index');
    }
}

